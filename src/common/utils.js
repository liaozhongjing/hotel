import Vue from 'vue'
//再原有的时间上加上几天，用于续约房间
function dateForm(startDate, day) {
    //一天的毫秒数
    let m = 86400000
    //获取开始时间的总毫秒数
    let total_m = +new Date(startDate)
    //计算得出新的总毫秒数
    let new_total_m = total_m + (day * m)
    let data = new Date(new_total_m)

    let y = data.getFullYear()
    let M = data.getMonth() + 1 >= 10 ? data.getMonth() + 1 : '0' + (data.getMonth() + 1)
    let d = data.getDate() >= 10 ? data.getDate() : '0' + data.getDate()

    let h = data.getHours() >= 10 ? data.getHours() : '0' + data.getHours()
    let mm = data.getMinutes() >= 10 ? data.getMinutes() : '0' + data.getMinutes()
    let ss = data.getSeconds() >= 10 ? data.getSeconds() : '0' + data.getSeconds()

    return `${y}-${M}-${d} ${h}:${mm}:${ss}`


}

//将2021-10-28T16:00:00.000Z 格式转化为 2020-09-30 02:02:02 
function dayjs(time) {
    let data = Vue.prototype.dayjs(time).format()
    // console.log(data);
    let arr = data.split('T')
    let y = arr[0]
    let arr2 = arr[1].split('+')
    let h = arr2[0]
    // console.log(y);
    // console.log(arr2);
    return y + ' ' + h
}

//生成当时时间
function getNowTime() {
    const data = new Date()

    let y = data.getFullYear()
    let M = data.getMonth() + 1 >= 10 ? data.getMonth() + 1 : '0' + (data.getMonth() + 1)
    let d = data.getDate() >= 10 ? data.getDate() : '0' + data.getDate()

    let h = data.getHours() >= 10 ? data.getHours() : '0' + data.getHours()
    let mm = data.getMinutes() >= 10 ? data.getMinutes() : '0' + data.getMinutes()
    let ss = data.getSeconds() >= 10 ? data.getSeconds() : '0' + data.getSeconds()
    return `${y}-${M}-${d} ${h}:${mm}:${ss}`
}

export { dateForm, dayjs, getNowTime }