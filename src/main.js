import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import registTips from 'components/common/Tips/index.js'

//引入时间转化插件
//将2021-10-28T16:00:00.000Z 格式转化为 2020-09-30 02:02:02 
import dayjs from 'dayjs'
Vue.prototype.dayjs = dayjs

Vue.config.productionTip = false
//全局注册提示组件
Vue.use(registTips)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
