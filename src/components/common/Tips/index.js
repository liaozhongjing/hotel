import Vue from 'vue'
import Tips from './Tips.vue'

//利用组件构造器构造出vue实例组件
const TipsConstructor = Vue.extend(Tips)

//显示提示框函数
function showTips({ msg, color }) {
    const tipsDom = new TipsConstructor({
        el: document.createElement('div'),
        data() {
            return {
                msg: msg,
                isShow: true,
                color: color,

            }
        }
    })
    //添加节点
    document.body.appendChild(tipsDom.$el)

    //显示时间限制
    setTimeout(() => {
        tipsDom.isShow = false
    }, 2000);
}

//全局注册
function registTips() {
    Vue.prototype.$tips = showTips
}

export default registTips