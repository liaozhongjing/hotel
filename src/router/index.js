import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/index'
  },
  {
    path: '/index',
    component: () => import('views/Index.vue')
  }
  ,
  {
    path: '/room',
    component: () => import('views/Room.vue')
  },
  {
    path: '/login',
    component: () => import('views/Login.vue')
  },
  {
    path: '/register',
    component: () => import('views/Register.vue')
  },
  {
    path: '/room_detail',
    component: () => import('views/Room_detail.vue')
  }
  ,
  {
    path: '/order',
    component: () => import('views/Order.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    // 始终滚动到顶部
    return { y: 0 }
  },
})

//解决vue-router3.0版本以上重复路由报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

export default router
